#[derive(Debug)]
struct Structure(i32);

#[derive(Debug)]
struct Deep(Structure);

#[derive(Debug)]
// generic params and lifetime elision
struct Person<'a> {
    name: &'a str,
    age: u8
}

#[derive(Debug)]
struct PersonTwo {
    name: String,
    age: u8
}

fn main() {
    println!("debug ..");
    println!("Now {:?} will print!", Structure(3));
    println!("Now {:?} will print!", Deep(Structure(7)));

    
    let name = "Peter";
    let age = 27;
    let peter = Person {name, age};

    println!("{:#?}", peter);

    let name = String::from("Kenneth");
    let age = 33;
    let peter2 = PersonTwo{name , age};
    println!("{:?}", peter2);
}