fn main() {
    let logical: bool = true;
    
    let a_float: f64 = 1.0; // Regular annotation
    let a_integer = 5i32; // Suffix annotation
    println!("{:?}", logical);
    println!("{:?}", a_integer);
    println!("{:?}", a_float);

    let default_float = 3.0; // 'f64'
    println!("{:?}", default_float);
    let default_integer = 7; // 'f32'
    println!("{:?}", default_integer);
    let mut inferred_type = 12;
    println!(">> {:?}", inferred_type);
    inferred_type = 4294967296i64;
    println!("{:?}", inferred_type);

    let mut mutable = 12;
    mutable = 12;
    println!("{:?}", mutable);
    //mutable= true;
    // overwritten with shadowing
    let mutable = true;
    println!("{:?}", mutable);
}