fn main() {
    // In general the {} will be automatically replace with any arguments  these will be stringified
    println!("{} days", 31);

    println!("{0}, this is {1}. {1}, this is {0}", "Alice", "Bob");

    println!("{subject} {verb} {object}",
            object="the lazy dog",
            subject="the quick brown fox",
            verb="jump over");
    println!("{} of {:b} people know binary, the other half doesn't", 1, 2);
    // pad six zeros in front of the 1 
    println!("{number:>width$}", number=1, width=6);

    println!("My name is {0}, {1} {0}", "Bond", "James");

    #[allow(dead_code)]
    struct Structure(i32);

    //println!("This struct `{}` won't print..", Structure(3));

    println!("Pi is roughly {:.3}", 22./7.)
}