fn main() {
    // line comment
    /*
    * This is an another type of comment, a block comment
    *
    */
    let x = 5 + /* 90 + */ 5;
    println!("Is `x` 10 or 100? x = {}", x);
}