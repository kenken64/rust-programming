<img src="https://static.javatpoint.com/tutorial/rust/images/rust-tutorial.jpg"/>

## Hello World

Print out the std out.

```
$ cd hello-world
$ Cargo run
```

## Comments

* Regular comments
* Doc comments

```
$ cd comments
$ Cargo run
```

## Formatting

* General formatting
* With suffix
* Positional arguments
* Named arguments
* Special formatting specify after :
* Right align with sepcified width
* Padding

```
$ cd formatting
$ Cargo run 
```

## Display

* fmt::Debug
* fmt::Display
* fmt::Binary

```
$ cd display
$ Cargo run
```

## Display Formatting

* format!("{}", foo) -> "3735928559"
* format!("0x{:X}", foo) -> "0xDEADBEEF"
* format!("0o{:o}", foo) -> "0o33653337357"

```
$ cd display-formatting
$ Cargo run
```

## Primitives

### Scalar Type

* signed integers: i8, i16, i32, i64, i128 and isize (pointer size)
* unsigned integers: u8, u16, u32, u64, u128 and usize (pointer size)
* floating point: f32, f64
* char Unicode scalar values like 'a', 'α' and '∞' (4 bytes each)
* bool either true or false
* and the unit type (), whose only possible value is an empty tuple: ()

### Compound Type
* arrays like [1, 2, 3]
* tuples like (1, true)

```
$ cd primitives
$ Cargo run

```