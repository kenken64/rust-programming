use std::fmt::{self, Formatter, Display};

struct City {
    name: &'static str,
    lat : f32,
    lon: f32,
}

impl Display for City {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let lat_c = if self.lat >= 0.0 { 'N' } else { 'S' };
        let lon_c = if self.lon >= 0.0 { 'E' } else { 'w' };
        // write! is like format! but it will write the 
        // formatted string into a buffer 
        write!(f, "{}: {:.3}°{} {:.3}°{}",
            self.name, self.lat.abs(), lat_c , self.lon.abs(), lon_c)
    }
}

#[derive(Debug)]
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

fn main() {
    for city in [
        City { name: "Singapore", lat: 1.351616 , lon: 103.808053},
        City { name: "Finland", lat: 62.777754, lon: 26.199539},
        City { name: "Qatar", lat: 25.315079, lon: 51.191201}, 
    ].iter() {
        println!("{}", *city);
    }

    for color in [
        Color { red:128, green: 255, blue: 90},
        Color { red:0, green: 3, blue: 254},
        Color { red:0, green: 0, blue: 0},
    ].iter() {
        println!("{:?}", *color);
    }
}